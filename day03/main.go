package main

import (
	funcs "aoc2022"
	"bufio"
	"bytes"
	"fmt"
	"strings"
)

func main() {
	testData := []byte(`vJrwpWtwJgWrhcsFMMfFFhFp
jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL
PmmdzqPrVvPwwTWBwg
wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn
ttgJtRGJQctTZtZT
CrZsJsPPZsGzwwsLwLmpwMDw`)

	testScore1 := solvePart1(testData)
	fmt.Println("Test score: ", testScore1)

	data, err := funcs.ReadInputFile("day03/input")
	if err != nil {
		panic(err)
	}

	part1Score := solvePart1(data)
	fmt.Println("Score part1: ", part1Score)

	testScore2 := solvePart2(testData)

	fmt.Println("Test score part2: ", testScore2)

	part2Score := solvePart2(data)
	fmt.Println("Score part2: ", part2Score)
}

func solvePart1(data []byte) int {
	r := bytes.NewReader(data)
	scan := bufio.NewScanner(r)

	score := 0
	for scan.Scan() {
		line := scan.Text()
		doubleItems := part1(line)
		for key := range doubleItems {
			score += calcLetter(key)
		}
	}

	return score
}

func solvePart2(data []byte) int {
	r := bytes.NewReader(data)
	scan := bufio.NewScanner(r)

	var batch []string

	score := 0
	for scan.Scan() {
		line := scan.Text()

		batch = append(batch, line)
		if len(batch)%3 == 0 {
			badge := part2(batch)
			//fmt.Println("Badge: ", string(badge))
			score += calcLetter(badge)
			batch = []string{}
		}
	}

	return score
}

func part2(batch []string) int {
	//fmt.Println(batch)

	var initSack string

	for _, char := range batch[1] {
		if strings.Index(batch[0], string(char)) >= 0 {
			initSack += string(char)
		}
	}

	//fmt.Println("initSack: ", initSack)

	var badge []int

	for _, char := range batch[2] {
		if strings.Index(initSack, string(char)) >= 0 {
			badge = append(badge, int(char))
		}
	}

	if len(badge) == 0 {
		panic(fmt.Sprintf("No badge found for batch: %v", batch))
	}

	if len(badge) > 1 {
		for _, val := range badge {
			if val != badge[0] {
				panic("Badge should contain only same chars")
			}
		}
	}

	return badge[0]
}

func part1(line string) map[int]bool {
	//fmt.Printf("Line: %q\n", line)

	compartmentSize := len(line) / 2
	//fmt.Println("Compart size size: ", compartmentSize)

	rs1 := make(map[int]bool)

	doubleItems := make(map[int]bool)

	for i := 0; i < compartmentSize; i++ {
		rs1[int(line[i])] = true
	}
	for i := compartmentSize; i < compartmentSize*2; i++ {
		if rs1[int(line[i])] {
			//fmt.Println("Adding double item: ", string(line[i]))
			doubleItems[int(line[i])] = true
		}
	}

	//fmt.Println("Double items: ", doubleItems)

	return doubleItems
}

func calcLetter(item int) int {
	if item > 91 {
		//fmt.Printf("Letter %v, score: %v\n", string(item), item-96)
		return item - 96
	}

	//fmt.Printf("Letter %v, score: %v\n", string(item), item-38)

	return item - 38
}

func letterByte() {
	fmt.Println("a", []byte(`a`))
	fmt.Println("z", []byte(`z`))
	fmt.Println("A", []byte(`A`))
	fmt.Println("Z", []byte(`Z`))
}
