package main

import (
	funcs "aoc2022"
	"bufio"
	"bytes"
	"fmt"
	"sort"
	"strconv"
)

func main() {
	data, err := funcs.ReadInputFile("day01/input")
	if err != nil {
		panic(err)
	}

	r := bytes.NewReader(data)
	scan := bufio.NewScanner(r)

	elfMap := make(map[int]int)
	elfCount := 0

	for scan.Scan() {
		line := scan.Text()
		if len(line) == 0 {
			elfCount++
			continue
		}
		amount, err := strconv.Atoi(line)
		if err != nil {
			fmt.Println("Not an int! ", err)
			continue
		}
		elfMap[elfCount] = elfMap[elfCount] + amount
	}

	max := 0
	elfMax := 0

	// Create array for easy sorting
	amountArr := make([]int, len(elfMap))

	for elf, amount := range elfMap {
		if amount > max {
			max = amount
			elfMax = elf
		}

		amountArr = append(amountArr, amount)
	}

	fmt.Printf("Answer part 1: elf %v has the highest amount of %v\n", elfMax, max)

	sort.Ints(amountArr)

	totalHighest := 0
	for i := 0; i < 3; i++ {
		totalHighest += amountArr[len(amountArr)-1-i]
	}

	fmt.Println("Answer part 2: total = ", totalHighest)
}
