package main

import (
	funcs "aoc2022"
	"bufio"
	"bytes"
	"fmt"
	"strconv"
	"strings"
)

func main() {
	testData := []byte(`2-4,6-8
2-3,4-5
5-7,7-9
2-8,3-7
6-6,4-6
2-6,4-8`)

	testScore1 := solvePart1(testData)
	fmt.Println("Test score part 1: ", testScore1)

	data, err := funcs.ReadInputFile("day04/input")
	if err != nil {
		panic(err)
	}

	part1Score := solvePart1(data)
	fmt.Println("Score part1: ", part1Score)

	testScore2 := solvePart2(testData)
	fmt.Println("Test score part 2: ", testScore2)

	part2Score := solvePart2(data)
	fmt.Println("Score part2: ", part2Score)

}

func solvePart1(data []byte) int {
	r := bytes.NewReader(data)
	scan := bufio.NewScanner(r)

	score := 0
	for scan.Scan() {
		line := scan.Text()
		elf1, elf2 := lineToPair(line)
		if checkTotalOverlap(elf1, elf2) {
			score++
		}
	}

	return score
}

func solvePart2(data []byte) int {
	r := bytes.NewReader(data)
	scan := bufio.NewScanner(r)

	score := 0
	for scan.Scan() {
		line := scan.Text()
		elf1, elf2 := lineToPair(line)
		if !noOverlap(elf1, elf2) {
			score++
		}
	}

	return score
}

func lineToPair(line string) (elf1, elf2 []int) {
	pairs := strings.Split(line, ",")
	e1 := strings.Split(pairs[0], "-")
	e2 := strings.Split(pairs[1], "-")
	elf1 = []int{cnvrt(e1[0]), cnvrt(e1[1])}
	elf2 = []int{cnvrt(e2[0]), cnvrt(e2[1])}

	return
}

func cnvrt(in string) int {
	val, err := strconv.Atoi(in)
	if err != nil {
		panic(err)
	}
	return val
}

func noOverlap(elfOne, elfTwo []int) bool {
	if elfOne[0] < elfTwo[0] && elfOne[1] < elfTwo[0] {
		//fmt.Println("No overlap: ", elfOne, elfTwo)
		return true
	}
	if elfOne[0] > elfTwo[1] && elfOne[1] > elfTwo[1] {
		//fmt.Println("No overlap: ", elfOne, elfTwo)
		return true
	}

	//fmt.Println("Has overlap: ", elfOne, elfTwo)

	return false
}

func checkTotalOverlap(elfOne, elfTwo []int) bool {
	if elfOne[0] >= elfTwo[0] && elfOne[1] <= elfTwo[1] {
		//fmt.Println("Elf one is within range elf two: ", elfOne, elfTwo)
		return true
	}
	if elfTwo[0] >= elfOne[0] && elfTwo[1] <= elfOne[1] {
		//fmt.Println("Elf two is within range elf one: ", elfOne, elfTwo)
		return true
	}

	return false
}
