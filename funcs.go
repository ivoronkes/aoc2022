package funcs

import (
	"os"
	"path"
)

func ReadInputFile(fileName string) ([]byte, error) {
	pwd, err := os.Getwd()
	if err != nil {
		return nil, err
	}
	filePath := path.Join(pwd, fileName)

	dat, err := os.ReadFile(filePath)
	if err != nil {
		return nil, err
	}
	return dat, nil
}
