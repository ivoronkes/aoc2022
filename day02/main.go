package main

import (
	funcs "aoc2022"
	"bufio"
	"bytes"
	"fmt"
	"strings"
)

func main() {
	testData := []byte(`A Y
B X
C Z`)

	solve(testData)

	data, err := funcs.ReadInputFile("day02/input")
	if err != nil {
		panic(err)
	}

	solve(data)

}

func solve(data []byte) {
	r := bytes.NewReader(data)
	scan := bufio.NewScanner(r)

	totalScore1 := 0
	totalScore2 := 0
	for scan.Scan() {
		line := scan.Text()
		input := strings.Fields(line)

		totalScore1 += points1(input[0], input[1])
		totalScore2 += points2(input[0], input[1])
	}

	fmt.Println("Total part 1: ", totalScore1)
	fmt.Println("Total part 2: ", totalScore2)
}

func points1(choose, resp string) int {
	total := 0

	switch {
	case resp == "X":
		total += 1
	case resp == "Y":
		total += 2
	case resp == "Z":
		total += 3
	}

	// win
	switch {
	case choose == "A" && resp == "Y":
		total += 6
	case choose == "B" && resp == "Z":
		total += 6
	case choose == "C" && resp == "X":
		total += 6
	}

	// draw
	switch {
	case choose == "A" && resp == "X":
		total += 3
	case choose == "B" && resp == "Y":
		total += 3
	case choose == "C" && resp == "Z":
		total += 3
	}

	//fmt.Println("Subtotal: ", total)
	return total
}

func points2(choose, outcome string) int {
	total := 0

	switch {
	case outcome == "X":
		total += 0
		total += rps(choose, outcome)
	case outcome == "Y":
		total += 3
		total += rps(choose, outcome)
	case outcome == "Z":
		total += 6
		total += rps(choose, outcome)
	}

	//fmt.Println("Subtotal: ", total)
	return total
}

func rps(choose, outcome string) int {
	// lose
	if outcome == "X" {
		if choose == "A" {
			return 3
		}
		if choose == "B" {
			return 1
		}
		if choose == "C" {
			return 2
		}
	}

	// draw
	if outcome == "Y" {
		if choose == "A" {
			return 1
		}
		if choose == "B" {
			return 2
		}
		if choose == "C" {
			return 3
		}
	}

	// win
	if outcome == "Z" {
		if choose == "A" {
			return 2
		}
		if choose == "B" {
			return 3
		}
		if choose == "C" {
			return 1
		}
	}
	panic(fmt.Errorf("Should not reach this point"))
}
